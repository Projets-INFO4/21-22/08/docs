## Lundi 10 Janvier 2022

- Séance introductive.
- Constitution des groupes.
- Attribution des projets.

## Lundi 17 Janvier 2022

- Participation au cours d’introduction à LoRa et LoRaWAN organisé par Monsieur Didier Donsez, afin de découvrir de plus en plus ces technologies.

## Lundi 24 Janvier 2022

- Récupération du matériel nécessaire pour la réalisation de notre projet au FabLab :
    * NUCLEO-WB55
    * Grove Base Shield
    * LoRa-E5 Mini
    * STM32 Discovery
    * Grove - Dust Sensor
    * Grove - CO2 & Temperature & Humidity Sensor (SCD30)

- Rencontre des IESE3 et 4 au FabLab.

- Découverte du matériel récuperé + comment l'utiliser.

- Lecture des documentations détaillant et approfondissant le matériel en disposition.

## Lundi 31 Janvier 2022

- Réunion avec Monsieur Didier Donsez afin de discuter à propos du contexte du projet et avoir plus de clarifications et de détails concernant le travail qui doit être réalisé.

- Découvrir et s'approfondir dans le projet.

- Se familiariser et apprendre à se servir des différentes ressources du projet.

## Lundi 07 Février 2022

- Installation de la chaîne de développement pour RIOT OS pour STM32 : [installation.](https://stm32python.gitlab.io/fr-version-lora/build.html)

- Installation de STM32CubeIDE qui est le logiciel intégré de ST pour développer les microcontrôleurs STM32 (+ STM32CubeProgrammer).

- Installation de Arduino IDE.

- Démarrage du tutoriel : [LoRa-E5 Development Kit.](https://wiki.seeedstudio.com/LoRa_E5_Dev_Board/)

- Enregistrement des devices sur TTN et Campus IOT :

<div align="center">
<img alt="CampusIoT" src="images/CampusIoT.png" heigth="400px" width="800px">
</div>

<br>

## Lundi 14 Février 2022

- Chargement de l’interpréteur Micropython sur la carte P-Nucleo-WB55 + démarrage des tutoriels : [tutoriels.](https://stm32python.gitlab.io/fr-version-lora/tutoriel.html)

- :white_check_mark: Flashage de la carte STM32 F3 Discovery.

- Se familiariser avec les documentations des différents capteurs.

## Lundi 28 Février 2022

- Les soutenances de mi-parcours.

- :white_check_mark: Flashage de la carte LoRaE5 Mini :

<div align="center">
<img alt="LoRaE5 Mini" src="images/LoRaE5_Mini.jpeg" heigth="300px" width="400px">
</div>

<br>

- Démarrage des tutoriels : [Exercices simples avec RIOT OS pour la carte LoRa E5 Mini.](https://stm32python.gitlab.io/fr-version-lora/lora-e5-mini.html)

## Mardi 01 Mars 2022

- Démarrage du tutoriel : [Communication LoRa avec RIOT OS pour les cartes STM32WL55.](https://stm32python.gitlab.io/fr-version-lora/lora.html)

- :white_check_mark: Activation de la carte LoRaE5 :

    * `loramac set deveui 6789abcd00000001`
    * `loramac set appeui 6789abcdffffffff`
    * `loramac set appkey 6789abcd000000016789abcd00000001`
    * `loramac join otaa`

- :white_check_mark: Envoi de données sur le réseau LoRa :

<div align="center">
<img alt="lora1" src="images/lora1.png" heigth="700px" width="850px">
</div>

<br>

## Lundi 07 Mars 2022

- Démarrage du tutoriel : [Grove - Dust Sensor.](https://wiki.seeedstudio.com/Grove-Dust_Sensor/)

- :white_check_mark: Fonctionnement du Dust Sensor avec Arduino : [Code](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/08/docs/-/blob/main/code/Arduino_Code/Dust_sensor/sketch_mar07a/sketch_mar07a.ino)

<div align="center">
<img alt="dust_arduino" src="images/dust_arduino.png" heigth="700px" width="700px">
</div>

<br> <br>

<div align="center">
<img alt="dust" src="images/dust.jpeg" heigth="300px" width="300px">
</div>

<br>

- Poursuite de l'exploitation des documentations.

## Mardi 08 Mars 2022

- Création du code C pour faire fonctionner Dust Sensor avec RIOT :
    * :warning: Des problèmes et des difficultés pour faire fonctionner le capteur de particules !

- Exploitation des ressources disponibles afin de comprendre le fonctionnement du capteur et essayer de fixer les problèmes rencontrés.

## Lundi 14 Mars 2022

- Poursuite du travail sur le code C pour faire fonctionner Dust Sensor avec RIOT :
    * :ballot_box_with_check: Implémentation de la méthode `pulseIn()` non supporté par RIOT : [Code](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/08/docs/-/blob/main/code/Riot_code/Riot_code_dust_and_scd30/Using_Sensor_without_Sending_Data/Dust_Sensor/capteur/main.c)
    * :warning: Problème avec les GPIO ?

- Démarrage du tutoriel : [Grove - CO2 & Temperature & Humidity Sensor (SCD30).](https://wiki.seeedstudio.com/Grove-CO2_Temperature_Humidity_Sensor-SCD30/)

## Mardi 15 Mars 2022

- :white_check_mark: Fonctionnement du CO2 & Temperature & Humidity Sensor avec Arduino : [Code](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/08/docs/-/blob/main/code/Arduino_Code/SCD30_sensor/SCD30_Example/SCD30_Example.ino)

<div align="center">
<img alt="co2_arduino" src="images/co2_arduino_code.png" heigth="700px" width="700px">
</div>

<br> <br>

<div align="center">
<img alt="co2" src="images/co2_arduino.jpeg" heigth="300px" width="300px">
</div>

<br>

- Démarrage du travail sur le capteur CO2 pour le faire fonctionner avec RIOT : <br>
    -> :eight_pointed_black_star: Travail à continuer !

## Lundi 21 Mars 2022

- :white_check_mark: Fonctionnement du CO2 & Temperature & Humidity Sensor avec RIOT : [Code](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/08/docs/-/blob/main/code/Riot_code/Riot_code_dust_and_scd30/Using_Sensor_without_Sending_Data/SDC30_Sensor/driver_scd30/main.c)

<div align="center">
<img alt="co2_riot" src="images/co2_riot_code.png" heigth="800px" width="800px">
</div>

<br> <br>

<div align="center">
<img alt="co2" src="images/co2_riot.jpeg" heigth="400px" width="400px">
</div>

<br>

- Démarrage du travail avec Cayenne (Compréhension + Exploitation).

## Mardi 22 Mars 2022

- :white_check_mark: Fonctionnement du Dust Sensor avec RIOT : [Code](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/08/docs/-/blob/main/code/Riot_code/Riot_code_dust_and_scd30/Using_Sensor_without_Sending_Data/Dust_Sensor/capteur/main.c)

<div align="center">
<img alt="dust_riot" src="images/dust_riot.png" heigth="700px" width="700px">
</div>

<br> <br>

<div align="center">
<img alt="dust2" src="images/dust2.jpeg" heigth="400px" width="400px">
</div>

<br>

- Poursuite du travail sur Cayenne (encodage des données capturées afin de les envoyer sur le réseau LoRaWAN) : <br>
    -> :eight_pointed_black_star: Travail à continuer !

## Lundi 28 Mars 2022

- Finalisation du travail sur Cayenne.

- :white_check_mark: Envoi des données des capteurs sur le réseau LoRaWAN (observation des données des capteurs affichées sur Cayenne) :

- __Dust Sensor__ : [Code](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/08/docs/-/blob/main/code/Riot_code/Riot_code_dust_and_scd30/Sending_sensor's_Data_With_Lora/Dust_Sensor/lora_dust/main.c)

<div align="center">
<img alt="dust_cayenne" src="images/dust_cayenne.png" heigth="800px" width="800px">
</div>

<br>

- __CO2 & Temperature & Humidity Sensor (SCD30)__ : [Code](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/08/docs/-/blob/main/code/Riot_code/Riot_code_dust_and_scd30/Sending_sensor's_Data_With_Lora/SCD30_Sensor/lora_co2_scd30/main.c)

<div align="center">
<img alt="co2_cayenne" src="images/co2_cayenne.png" heigth="800px" width="800px">
</div>

<br> <br>

<div align="center">
<img alt="cayenne" src="images/cayenne.png" heigth="800px" width="800px">
</div>

<br>

## Mardi 29 Mars 2022

- Finalisation du travail + vérfication globale du projet.

- Rédaction des sections manquantes sur le site web concernant les capteurs sur lesquels on a travaillé durant le projet (Dust Sensor + CO2 & Temperature & Humidity Sensor) : [Site Web](https://stm32python.gitlab.io/fr-version-lora/riot_sensors.html#lecture-de-la-qualit%C3%A9-de-lair)
